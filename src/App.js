import React, {Component} from 'react';

import './App.css';
import Header from './Components/Header';
import Footer from './Components/Footer';
import ProductList from './Components/ProductList';
import Cart from './Components/Cart';



class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="container">
          <ProductList/>
          <Cart/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
