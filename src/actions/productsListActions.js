import {productsUrl} from '../config';
import {PRODUCTS_FILTER_LIST, PRODUCTS_FETCHING, PRODUCTS_DONE} from '../reducers/productsReducer';

const fetching = () => ({
  type: PRODUCTS_FETCHING
});

const done = (data) => ({
  type: PRODUCTS_DONE,
  payload: [...data]
});

export const fetchProducts = () => dispatch => {
  dispatch(fetching());

  fetch(productsUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(response => {
    response.json().then(data => {
      dispatch(done(data));
    }).catch(err => {console.log(err)})
  })
}

export const filterList = (value) => {
  return {
    type: PRODUCTS_FILTER_LIST,
    payload: { value: value }
  }
}