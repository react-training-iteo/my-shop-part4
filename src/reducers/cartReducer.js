export const CART_ADD_ITEM = 'CART_ADD_ITEM';
export const CART_REMOVE_ITEM = 'CART_REMOVE_ITEM';

const actions = {
  [CART_ADD_ITEM]: (state, action) => {
    const { payload: { item } } = action;

    const myProducts = [...state.myProducts, item];

    return {
      ...state,
      myProducts: myProducts,
      sum: myProducts.length > 0 ? myProducts.map(v => v.price).reduce((a, b) => a + b) : 0
    }
  },
  [CART_REMOVE_ITEM]: (state, action) => {
    const { payload: { id } } = action;

    const myProducts = [...state.myProducts].filter(v => v.id !== id);

    return {
      ...state,
      myProducts: myProducts,
      sum: myProducts.length > 0 ? myProducts.map(v => v.price).reduce((a, b) => a + b) : 0
    }
  }
}

const initialState = {
  myProducts: [],
  sum: 0
}

export default (state = initialState, action) => {
  const {type} = action;

  if(actions[type]) return actions[type](state, action);
  return state;
}
