import { products } from '../constants/productListConst';

export const PRODUCTS_FILTER_LIST = 'PRODUCTS_FILTER_LIST';
export const PRODUCTS_FETCHING = 'PRODUCTS_FETCHING';
export const PRODUCTS_DONE = 'PRODUCTS_DONE';

const actions = {
  [PRODUCTS_DONE]: (state, action) => {
    const {payload} = action;

    return {
      ...state,
      productsList: payload,
      filteredData: payload,
      loading: false
    }
  },
  [PRODUCTS_FETCHING]: (state, action) => {
    return {
      ...state,
      loading: true
    }
  },
  [PRODUCTS_FILTER_LIST]: (state, action) => {
    return {
      ...state,
      filteredData: state.productsList.filter(v => v.name.toLowerCase().startsWith(action.payload.value))
    }
  },
}

const initialState = {
  loading: false,
  productsList: [],
  filteredData: []
}

export default (state = initialState, action) => {
  const { type } = action;

  if(actions[type]) return actions[type](state, action);
  return state;
}