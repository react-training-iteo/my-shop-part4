import React from 'react';
import './Product.css';

class Product extends React.Component {
  render() {
    const {id, name, description, photo, price, stock, addToCart} = this.props;

    return (
      <div className="product-box">
        <div>
          <img className="product-box--image" src={photo} />
        </div>
        <div>
          <h2>{name}</h2>
          <pre>Cena: {`${price}zł`}</pre>
          <p>{description}</p>
          <button disabled={!stock} onClick={() => addToCart(id)}>Dodaj do koszyka</button>
        </div>
      </div>
    )
  }
}

Product.defaultProps = {
  name: '',
  description: '',
  price: 0,
  stock: false
}

export default Product;
