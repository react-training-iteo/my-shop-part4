import React from 'react';
import './SearchBox.css';

class SearchBox extends React.Component {

  searchProduct() {
    const {filterList} = this.props;
    const {value} = this.input;

    filterList(value);
  }

  render() {
    return (
      <div className="search">
        <input onChange={()=>this.searchProduct()} ref={input => {this.input = input}} className="search-input" placeholder="Szukaj..."/>
      </div>
    )
  }
}

export default SearchBox;
