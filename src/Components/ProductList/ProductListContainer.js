import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ProductList from './ProductList';
import {addItem} from '../../actions/cartActions';
import {filterList, fetchProducts} from '../../actions/productsListActions';

const mapStateToProps = state => ({
  products: state.products
})

const mapDispatchToProps = dispatch => ({
  addToCart: bindActionCreators(addItem, dispatch),
  filterList: bindActionCreators(filterList, dispatch),
  fetchProducts: bindActionCreators(fetchProducts, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);