import React from 'react';
import './ProductList.css';
import SearchBox from './Components/SearchBox';
import Product from './Components/Product';

class ProductList extends React.Component {
  componentDidMount(){
    const {fetchProducts} = this.props;
    fetchProducts();
  }

  addItem = (id) => {
    const { products: { productsList }, addToCart } = this.props;
    addToCart(productsList.find(v => v.id === id));
  }

  getProducts() {
    const { products: { filteredData, loading } } = this.props;

    if(loading) return <div>Ładowanie...</div>

    return filteredData.map((value, index) => (
      <Product 
        key={index} 
        id={value.id} 
        name={value.name} 
        photo={value.photo} 
        price={value.price} 
        stock={value.in_stock} 
        description={value.description} 
        addToCart={this.addItem} 
      />
    ))
  }

  render() {
    const {filterList} = this.props;

    return (
      <section className="product-list">
        <SearchBox filterList={filterList} />
        {this.getProducts()}
      </section>
    )
  }
}
ProductList.defaultProps = {
  products: {
    filteredData: []
  },
  addToCart: () => {},
  searchText: () => {}
}

export default ProductList;
