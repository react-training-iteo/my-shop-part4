import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Cart from './Cart';
import { removeItem } from '../../actions/cartActions';

const mapStateToProps = state => ({
  cart: state.cart
})

const mapDispatchToProps = dispatch => ({
  removeItem: bindActionCreators(removeItem, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart)