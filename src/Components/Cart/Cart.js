import React from 'react';
import './Cart.css';

import SelectedProduct from './Components/SelectedProducts';
import TotalPrice from './Components/TotalPrice'

class Cart extends React.Component {

  showProductsInCart() {
    const { cart: { myProducts }, removeItem } = this.props;

    return myProducts.map((value, index) => {
      return (
        <SelectedProduct key={index} id={index} productId={value.id} name={value.name} price={value.price} removeFromCart={removeItem} />
      )
    })
  }

  render() {
    const { cart: { sum } } = this.props;

    return (
      <section className="user-cart">
        <h4>Twój koszyk</h4>
        <ul className="user-cart--list">
          {this.showProductsInCart()}
        </ul>
        <TotalPrice price={sum}/>
      </section>
      )
  }
}

Cart.defaultProps = {
  cart: {
    myProducts: [],
    sum: 0
  }
}

export default Cart;
